#include "sample.h"
#include "gtest/gtest.h"

TEST(AddOneTest, Normal) {
  EXPECT_EQ(1, addOne(0));
  EXPECT_GT(2, addOne(0));
  EXPECT_LT(1, addOne(1));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
